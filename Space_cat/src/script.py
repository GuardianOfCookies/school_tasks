
import pwnlib.tubes.server as hope
import random
import time

flag='UralCTF{49fe100103cbc466c7ba22c373636a56d}'
index=0
cat='''   /\_/\ \n   >^.^<.---.\n  _'-`-'     )\ \n (6--\ |--\ (`.`-.\n     --'  --'  ``-'\n'''

ports=[port for port in range(46470,46480)]
next_port = None
current_port = None

def get_flag_part():
	global index
	part=flag[index:index+2]
	return part

def cb(r,port):
	global current_port
	global next_port

	if port == current_port:
		r.send(cat)
		r.send('Your part of the flag is:{0}\nNext port:{1}'.format( get_flag_part(), next_port ))
	else:
		r.send('No cats here...')
	r.close()

def main():
	global next_port
	global current_port
	global index
	
	for port in ports:
		hope.server(port, callback=cb)

	next_port=random.choice(ports)
	while(True):
		current_port=next_port
		print('CURRENT PORT: ' + str(next_port))
		next_port=random.choice(ports)
		print('NEXT PORT: ' + str(next_port))
		time.sleep(5)
		index = index+2 if index+3 < len(flag) else 0

main()	
