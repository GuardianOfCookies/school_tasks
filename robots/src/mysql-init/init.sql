CREATE DATABASE IF NOT EXISTS robots;

use robots;

CREATE TABLE IF NOT EXISTS users (
    user_id INT AUTO_INCREMENT,
    login VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY (user_id)
)  ENGINE=INNODB;

INSERT INTO users (login, password) VALUES ('admin', '70511d2883b4');
